#!/bin/bash -e

CHROMIUM_DIR="${CHROMIUM_DIR:-/srv/chromium}"
ROOT_DIR=$(dirname "$(readlink -f "$0")")
BUILDID="${CI_JOB_ID:-$(date +%s)}"
ARCH=arm64

function usage() {
    cat << EOT
  usage $0 [-hac]

  OPTIONS
    -h show this usage
    -a target build archictecture (default: arm)
    -c clean chromium repository
EOT
}

function init() {
    cd "${CHROMIUM_DIR}"
    if [ ! -f .gclient ]; then
        echo ">> [$(date)] Init project"
        fetch --nohooks android
    fi
    clean
    mkdir -p "${ROOT_DIR}/apks"
    cd "${CHROMIUM_DIR}/src"
    gclient sync
    gclient runhooks
}

function checkout() {
    if [ -z "${CHROMIUM_VERSION}" ] ; then
        CHROMIUM_VERSION=$(head -n 1 "${ROOT_DIR}/bromite/CHANGELOG.md" | sed 's/# \(.*\)/\1/')
        export CHROMIUM_VERSION
    fi

    echo ">> [$(date)] Use Chromium v${CHROMIUM_VERSION}"

    git fetch --tags
    git checkout -f "tags/${CHROMIUM_VERSION}"
    gclient sync --with_branch_heads --with_tags -D
}

function clean() {
    if [ -d "${CHROMIUM_DIR}/src" ]; then
        cd "${CHROMIUM_DIR}/src"
        git reset --hard && git clean -xfdf && gclient sync --with_branch_heads --with_tags -f -R -D
    fi
}

function setup() {
    echo ">> [$(date)] Install dependencies"
    build/install-build-deps-android.sh
    gclient sync --with_branch_heads --with_tags -D
}

function patch() {
    echo ">> [$(date)] Apply Bromite and /e/ patches"
    git am --abort
    PATCHES_LIST=$(cat "${ROOT_DIR}/build/e_patches_list.txt")
    for file in $PATCHES_LIST; do
        echo " -> Apply $file"
        git config --global user.name "John Doe"
        git config --global user.email "johndoe@example.com"
        git am < "${ROOT_DIR}/bromite/build/patches/$file"
        echo " "
    done
    cp -r -f -v "${ROOT_DIR}"/src/. "${CHROMIUM_DIR}"/src/
    rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_base/values/ic_launcher_round_alias.xml &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_base/drawable-v26/ic_launcher.xml &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_base/drawable-v26/ic_launcher_round.xml &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_chromium_base/mipmap-mdpi/layered_app_icon_background.png &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_chromium_base/mipmap-mdpi/layered_app_icon.png &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_chromium_base/mipmap-xhdpi/layered_app_icon_background.png &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_chromium_base/mipmap-xhdpi/layered_app_icon.png &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_chromium_base/mipmap-xxxhdpi/layered_app_icon_background.png &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_chromium_base/mipmap-xxxhdpi/layered_app_icon.png &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_chromium_base/mipmap-nodpi/layered_app_icon_foreground.xml &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_chromium_base/mipmap-hdpi/layered_app_icon_background.png &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_chromium_base/mipmap-hdpi/layered_app_icon.png &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_chromium_base/mipmap-xxhdpi/layered_app_icon_background.png &&
	rm -r -f -v "${CHROMIUM_DIR}"/src/chrome/android/java/res_chromium_base/mipmap-xxhdpi/layered_app_icon.png &&
    gclient runhooks
}

function build() {
    local out="out/${BUILDID}_${ARCH}"
    echo ">> [$(date)] Build for ${ARCH}"
    export NINJA_SUMMARIZE_BUILD=1
    gn gen --args="$(cat "${ROOT_DIR}"/build/browser.gn_args) target_cpu=\"${ARCH}\" " "$out"
    autoninja -C "$out" chrome_public_apk system_webview_apk
    cp "$out/apks/ChromePublic.apk" "${ROOT_DIR}/apks/ChromePublic_$ARCH.apk"
    cp "$out/apks/SystemWebView.apk" "${ROOT_DIR}/apks/SystemWebView_$ARCH.apk"
}

while getopts "ha:c" option; do
    case $option in
        a)
          ARCH="$OPTARG"
          ;;
        c)
          clean
          exit 0
          ;; 
        \?)
          echo "wrong option."
          usage
          exit 1
          ;;
        h)
          usage
          exit 0
          ;;
    esac
done
shift $((OPTIND - 1))

init
checkout
setup
patch
build 
