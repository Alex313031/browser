#!/bin/bash

# Copyright (c) 2022 Alex313031 and /e/OS Foundation.

YEL='\033[1;33m' # Yellow
CYA='\033[1;96m' # Cyan
RED='\033[1;31m' # Red
GRE='\033[1;32m' # Green
c0='\033[0m' # Reset Text
bold='\033[1m' # Bold Text
underline='\033[4m' # Underline Text

# Error handling
yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 111; }
try() { "$@" || die "${RED}Failed $*"; }

# --help
displayHelp () {
	printf "\n" &&
	printf "${bold}${GRE}Script to build Elixir locally on Linux.${c0}\n" &&
	printf "\n"
}

case $1 in
	--help) displayHelp; exit 0;;
esac

cd $HOME &&

git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git &&

echo "umask 022" >> $HOME/.bashrc &&
echo "export EDITOR=nano" >> $HOME/.bashrc &&
echo "export VISUAL=nano" >> $HOME/.bashrc &&
echo "export NINJA_SUMMARIZE_BUILD=1" >> $HOME/.bashrc &&
echo "export PATH=$HOME/depot_tools:\$PATH" >> $HOME/.bashrc &&

mkdir -p -v chromium && mkdir -p -v chromium/apks && cd chromium &&

fetch --nohooks android &&

cd chromium/src &&
build/install-build-deps-android.sh &&
git fetch --tags &&
gclient sync --with_branch_heads --with_tags &&

CHROMIUM_VERSION==$(head -n 1 "RELEASE" | sed 's/# \(.*\)/\1/') &&
export CHROMIUM_VERSION &&

git checkout "tags/${CHROMIUM_VERSION}" &&
gclient sync --with_branch_heads --with_tags -D &&

cd $HOME/browser &&
patch.sh &&

