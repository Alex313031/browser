#!/bin/bash -e

# Copyright (c) 2022 Alex313031 and /e/OS Foundation.

YEL='\033[1;33m' # Yellow
CYA='\033[1;96m' # Cyan
RED='\033[1;31m' # Red
GRE='\033[1;32m' # Green
c0='\033[0m' # Reset Text
bold='\033[1m' # Bold Text
underline='\033[4m' # Underline Text

# Error handling
yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 111; }
try() { "$@" || die "${RED}Failed $*"; }

# --help
displayHelp () {
	printf "\n" &&
	printf "${bold}${GRE}Script to apply Bromite and /e/OS patches over the Chromium source tree.${c0}\n" &&
	printf "${bold}${YEL}Use the --no-/e/OS flag to make a vanilla Bromite build.${c0}\n" &&
	printf "\n"
}

case $1 in
	--help) displayHelp; exit 0;;
esac

# About
printf "\n" &&
printf "${bold}${GRE}Script to apply Bromite and /e/OS patches over the Chromium source tree.${c0}\n" &&
printf "${bold}${YEL}Use the --no-/e/OS flag to make a vanilla Bromite build.${c0}\n" &&
printf "\n"

# Setup directories
mkdir -p -v $HOME/chromium/patches &&
mkdir -p -v $HOME/chromium/eos &&
printf "\n" &&

cp -r -f -v bromite/build/patches/. $HOME/chromium/patches/ &&
printf "\n" &&

cp -r -f -v build/e_patches_list.txt $HOME/chromium/patches/ &&
printf "\n" &&

cp -r -f -v ./src/. $HOME/chromium/eos/ &&
printf "\n" &&

cd $HOME/chromium/src &&
printf "\n" &&

printf "${YEL}Using bromite_patches_list.txt for patch list...${c0}\n" &&
printf "\n" &&

# Patch Chromium with Bromite patches
PATCHES_LIST=$(cat "$HOME/chromium/patches/e_patches_list.txt")

for file in $PATCHES_LIST; do
    echo " -> Apply $file"
    git config user.name "John Doe"
    git config user.email "johndoe@example.com"
    git am < "$HOME/chromium/patches/$file"
    echo " "
done &&

printf "\n" &&
printf "${YEL}Copying /e/OS Patches over the Chromium tree...${c0}\n" &&
printf "\n" &&

# Patch Chromium with /e/OS Patches

cp -r -f -v ../eos/. ../src/ &&

cd $HOME/chromium/src &&

#rm -r -f -v chrome/android/java/res_base/values/ic_launcher_round_alias.xml &&
rm -r -f -v chrome/android/java/res_base/drawable-v26/ic_launcher.xml &&
rm -r -f -v chrome/android/java/res_base/drawable-v26/ic_launcher_round.xml &&
rm -r -f -v chrome/android/java/res_chromium_base/mipmap-mdpi/layered_app_icon_background.png &&
rm -r -f -v chrome/android/java/res_chromium_base/mipmap-mdpi/layered_app_icon.png &&
rm -r -f -v chrome/android/java/res_chromium_base/mipmap-xhdpi/layered_app_icon_background.png &&
rm -r -f -v chrome/android/java/res_chromium_base/mipmap-xhdpi/layered_app_icon.png &&
rm -r -f -v chrome/android/java/res_chromium_base/mipmap-xxxhdpi/layered_app_icon_background.png &&
rm -r -f -v chrome/android/java/res_chromium_base/mipmap-xxxhdpi/layered_app_icon.png &&
rm -r -f -v chrome/android/java/res_chromium_base/mipmap-nodpi/layered_app_icon_foreground.xml &&
rm -r -f -v chrome/android/java/res_chromium_base/mipmap-hdpi/layered_app_icon_background.png &&
rm -r -f -v chrome/android/java/res_chromium_base/mipmap-hdpi/layered_app_icon.png &&
rm -r -f -v chrome/android/java/res_chromium_base/mipmap-xxhdpi/layered_app_icon_background.png &&
rm -r -f -v chrome/android/java/res_chromium_base/mipmap-xxhdpi/layered_app_icon.png &&

# Run final gclient runhooks
gclient runhooks
printf "\n" &&

printf "${GRE}Done! You can now set your args.gn and build Elixir.${c0}\n" &&
printf "\n"

exit 0
