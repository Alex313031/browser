package org.chromium.chrome.browser.provider;


import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import androidx.annotation.NonNull;

public class SearchEngineProvider extends ContentProvider {

    public static final String CONFIGURATION_PREFERENCE_FILE_NAME = "foundation.e.browser_preferences";

    public static final String AUTHORITY = "foundation.e.browser.provider";

    private static final int MATCH_DATA = 0x010000;

    private static UriMatcher matcher;

    private static final String PREFERENCE_KEY = "org.chromium.chrome.browser.searchwidget.SEARCH_ENGINE_SHORTNAME";

    private static void init(){
        matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, "search_engine", MATCH_DATA);
    }

    @Override
    public boolean onCreate() {
        if(matcher == null){
            init();
        }
        return true;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd." + AUTHORITY + ".item";
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        //Provide a read only access to the content provider
        MatrixCursor cursor;
        if (matcher.match(uri) == MATCH_DATA) {
            cursor = new MatrixCursor(new String[]{PREFERENCE_KEY});
            MatrixCursor.RowBuilder rowBuilder = cursor.newRow();
            SharedPreferences sharedPreferences = getContext().getSharedPreferences(
                    CONFIGURATION_PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
            if (sharedPreferences != null) {
                rowBuilder.add(sharedPreferences.getString(PREFERENCE_KEY, ""));
            } else {
                rowBuilder.add("");
            }
        } else {
            throw new IllegalArgumentException("Unsupported uri " + uri);
        }
        return cursor;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }
}
