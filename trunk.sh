#!/bin/bash

# Copyright (c) 2022 Alex313031, Midzer, and /e/OS Foundation.

YEL='\033[1;33m' # Yellow
CYA='\033[1;96m' # Cyan
RED='\033[1;31m' # Red
GRE='\033[1;32m' # Green
c0='\033[0m' # Reset Text
bold='\033[1m' # Bold Text
underline='\033[4m' # Underline Text

# Error handling
yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 111; }
try() { "$@" || die "${RED}Failed $*"; }

# --help
displayHelp () {
	printf "\n" &&
	printf "${bold}${GRE}Script to Rebase/Sync Chromium repo on Linux.${c0}\n" &&
	printf "${bold}${YEL}Use the --shallow flag to do a shallow sync, if you have downloaded${c0}\n" &&
	printf "${bold}${YEL}the Chromium repo with the --no-history flag.${c0}\n" &&
	printf "\n"
}

case $1 in
	--help) displayHelp; exit 0;;
esac

printf "\n" &&
printf "${bold}${GRE}Script to Rebase/Sync Chromium repo on Linux.${c0}\n" &&
printf "\n" &&
printf "${YEL}Rebasing/Syncing and running hooks...\n" &&
tput sgr0 &&

cd $HOME/chromium/src/v8/ &&

git checkout -f origin/main &&

cd $HOME/chromium/src/third_party/devtools-frontend/src &&

git checkout -f origin/main &&

cd $HOME/chromium/src &&

rm -v -f $HOME/chromium/src/chrome/android/java/res_base/drawable-v26/ic_launcher_round_alias.xml &&

rm -v -f $HOME/chromium/src/chrome/android/java/src/org/chromium/chrome/browser/provider/SearchEngineProvider.java &&

rm -v -f $HOME/chromium/src/chrome/browser/ui/android/strings/foundation_e.grdp &&

rm -v -f $HOME/chromium/src/components/ntp_tiles/resources/foundation_e.json &&

rm -v -r -f $HOME/chromium/src/third_party/ub-uiautomator/ &&

git checkout -f origin/main &&

git rebase-update &&

git fetch --tags &&

gclient sync --with_branch_heads --with_tags -f -R -D &&

gclient runhooks &&

alias gsync='gclient sync --with_branch_heads --with_tags' &&

printf "${YEL}Done!\n" &&
printf "\n" &&
tput sgr0 &&

exit 0
